#!/bin/bash

SOURCE=~/repos/dotfiles/bashrc/templates/append_me_to_bashrc.template.bash
DEST=~/.bashrc
RESULT="success"

if [ "$CUSTOM_SHELL_ALREADY_INSTALLED" == "true" ]; then
    printf "Nothing to do - already done:\n"
    printf "ERROR: File '$DEST' already containts '$SOURCE'\n"
    printf "\n"
    RESULT="error"
    exit 0
fi

if [ ! -f $SOURCE ]; then
    printf "ERROR: Could not find source file \'$SOURCE\'\n"
    RESULT="error"
fi

if [ ! -f $DEST ]; then
    printf "ERROR: Could not find destination file \'$DEST\'\n"
    RESULT="error"
fi

if [ "$RESULT" == "success" ]; then
    cat $SOURCE >> $DEST
    printf "Success: File '$DEST' has been modified.\n"
else
    printf "Installation aborted.\n"
fi

