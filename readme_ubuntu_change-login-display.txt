From: ChatGPT

To change the display monitor where the login screen appears on your Ubuntu
desktop computer, you can follow these steps:

1. Identify Monitors:

   First, identify the names or identifiers of your displays. You can do this
   by opening a terminal (press Ctrl + Alt + T) and running the following command:

       xrandr --listmonitors

   This command will list the connected monitors along with their names
   (e.g., HDMI-1, DP-1, etc.).


2. Create a Monitor Configuration File:

   Create a configuration file for your display settings using xrandr command.
   Open a terminal and run:

   sudo nano /usr/share/X11/xorg.conf.d/20-monitor.conf

   In this configuration file, you can specify the display layout you want.
   For example, to set a specific monitor (let's say HDMI-1) as primary, add
   the following lines to the file:

       Section "Monitor"
           Identifier "HDMI-1"
           Option "Primary" "true"
       EndSection

   Adjust the Identifier based on your monitor's name.


3. Reboot or Restart Display Manager:

   After saving the configuration file, reboot your computer or restart the
   display manager for the changes to take effect. You can restart the display
   manager by running:

       sudo systemctl restart display-manager.service


4. Test Login Screen Placement:

   After rebooting, check if the login screen appears on the desired monitor.
   If not, you may need to adjust your monitor configuration file further based
   on your setup.


5. Additional Steps (if needed):

   If you have a more complex setup or the above steps don't work, you might
   need to use additional tools like arandr (GUI for xrandr) to configure your
   display layout graphically. Install it with:

       sudo apt update
       sudo apt install arandr

    Open arandr from the applications menu, adjust your display layout, and
    save the configuration. Then, follow steps 3 and 4 again to apply the
    changes.


By following these steps, you should be able to control which monitor displays
the login screen on your Ubuntu desktop computer. Adjustments might be needed
based on your specific hardware setup and desktop environment.
