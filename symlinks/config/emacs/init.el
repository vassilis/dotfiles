;;; .emacs.d/init.el --- Emacs Configuration File

;;; Commentary:

;;; Code:

;; +============================================================================
;; |  WORKAROUND START
;; +============================================================================
;;
;; WARNING - TEMPORARY !!! REMOVE AFTER GPG KEYS ARE UPDATED!
;; This is a workaround if gpg keys have expired and cannot install
;; any packages from 'elpa' ('gnu').
;; To apply the workaround disable signature checking,
;; i.e., enable the following line:
;;
;;(setq package-check-signature nil)
;;
;; After applying this setting, install the gnu-elpa-keyring-update
;; package and update the keys.
;; 
;; Remember to re-enable signature verification afterwards.
;; The following enables signature checking and allows unsigned
;; package installation (default):
;;
;;(setq package-check-signature 'allow-unsigned)
;;
;; +============================================================================
;; |  WORKAROUND END
;; +============================================================================

;; +============================================================================
;; |  How to update all packages
;; +============================================================================
;;
;; # In summary:
;;    M-x package-list-packages <RET> U x
;;
;; # Detailed breakdown of commands:
;;
;; # This takes you to the *Packages* buffer and also updates the list of packages
;;     M-x package-list-packages
;; # This marks all packages that can be updated
;;     U
;; # This executes the package update
;;     x

;; +============================================================================
;; |  PACKAGE SUPPORT
;; +============================================================================
;; |  Based on:
;; |  https://stackoverflow.com/questions/10092322/how-to-automatically-install-emacs-packages-by-specifying-a-list-of-package-name
;; +============================================================================

;; ---------------------------------------------------------
;; List of package archives (repositories)
;; ---------------------------------------------------------
;; 'NonGNU' repo included by default
;; ---------------------------------------------------------
(require 'package)
(add-to-list 'package-archives '("gnu"   . "https://elpa.gnu.org/packages/") t)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"   ) t)

;; ---------------------------------------------------------
;; List of packages
;; ---------------------------------------------------------
(setq myPackages
      '(
	;; GPG keys used by the ELPA package manager have limited validity in time.
	;; This package is needed to make sure signature verification does not
	;; supriously fail when installing packages.
	;;
	gnu-elpa-keyring-update ; Update Emacs's GPG keyring for GNU ELPA

	company         ; Modular text completion framework
	flycheck        ; On-the-fly syntax checking
	projectile      ; Manage and navigate projects in Emacs easily

	;; --------------------------------------
	;; On the fly syntax checking, if you prefer
	;; the more popular flycheck over renewed flymake
	;; --------------------------------------
	;;flycheck
	;;use-package   ; built-in, no need to install
	dap-mode        ; if your language is supported by the debugger

	;; --------------------------------------
	;; Set up some better Emacs defaults
	;; --------------------------------------
	;;better-defaults

	;; ---------------------------------------
	;; LSP - Language Server Protocol support for EMACS:
	;; https://emacs-lsp.github.io/lsp-mode/
	;; ---------------------------------------
	lsp-ui          ; fancy sideline, popup documentation, VScode-like peek UI, etc.
	;;helm-lsp      ; on type completion alternative of xref-apropos using helm
	;;lsp-ivy       ; on type completion alternative of xref-apropos using ivy
	;;

	;; ---------------------------------------
	;; Python:
	;; ---------------------------------------
	elpy            ; Emacs Lisp Python Environment
	;;py-autopep8   ; Run autopep8 on save
	;;blacken       ; Black formatting on save
	;;paredit       ; Structured editing of S-expressions

	;; ---------------------------------------
	;; Themes:
	;; ---------------------------------------
	;;gruvbox-theme
	;;monokai-theme
	;;tangotango-theme

	;; ---------------------------------------
	;; Git
	;; ---------------------------------------
	magit           ; A Git procelain inside Emacs

	;; ---------------------------------------
	;; Dependencies
	;;
	;; The following are automatically added,
	;; don't need to install explicitly
	;; ---------------------------------------
	;;lsp-mode      ; enable LSP
	;;lsp-treemacs  ; various tree based UI controls (symbols, errors overview, call hierarchy, etc.)

	))

;; ---------------------------------------------------------
;; FUNCTION DEFINITION:
;; interactive-package-installation
;; ---------------------------------------------------------
;; Scan the package list provided as argument
;; If the package listed is not already installed, ask the
;; user if it should be installed
;; ---------------------------------------------------------
(defun interactive-package-installation (&rest packages)
  "Assure every package is installed, ask for installation if it’s not.

Return a list of installed packages or nil for every skipped package."
  (mapcar
   (lambda (package)
     ;; (package-installed-p 'evil)
     (if (package-installed-p package)
	 nil
       (if (y-or-n-p (format "Package %s is missing. Install it? " package))
	   (package-install package)
	 package)))
   packages))

;; ---------------------------------------------------------
;; FUNCTION DEFINITION:
;; automatic-package-installation
;; ---------------------------------------------------------
;; Scan the list in var 'myPackages'
;; If any package listed is not already installed, install it
;; ---------------------------------------------------------
(defun automatic-package-installation ()
  (dolist (package myPackages)
    (unless (package-installed-p package)
      (package-install package))))

;; ---------------------------------------------------------
;; FUNCTION DEFINITION:
;; automatic-package-installation-alternative
;; ---------------------------------------------------------
;; Scan the list in var 'myPackages'
;; If any package listed is not already installed, install it
;; ---------------------------------------------------------
(defun automatic-package-installation-alternative ()
  (mapc #'(lambda (package)
	    (unless (package-installed-p package)
	      (package-install package)))
	myPackages))

;; ---------------------------------------------------------
;; This must come before configurations of installed packages.
;; ---------------------------------------------------------
(package-initialize)

;; ---------------------------------------------------------
;; Fetch the list of availabe packages from the archives
;; ---------------------------------------------------------
(unless package-archive-contents
  (package-refresh-contents))

;; ---------------------------------------------------------
;; Do the installation
;; ---------------------------------------------------------
;; Use only one of the following options

;; ToDo: fix the interactive installation.
;; Argument list is expected as ticklist but 'myPackages' is not a ticklist
;; e.g. (interactive-package-installation 'package1 'package2 'package3)

;;;(interactive-package-installation myPackages)
;;;(automatic-package-installation)
(automatic-package-installation-alternative)


;; +============================================================================
;; |  Setup LSP (Language Server Protocol) options
;; +============================================================================

;; if you want to change prefix for 'lsp-mode' keybindings.
;;;(setq lsp-keymap-prefix "s-l")

;; Ensure lsp-mode has been installed
(require 'lsp-mode)

;; To defer LSP server startup (and DidOpen notifications) until
;; the buffer is visible, use #'lsp-deferred, otherwise use #'lsp
(add-hook    'XXX-mode-hook   #'lsp-deferred)
;;;(add-hook 'XXX-mode-hook   #'lsp         )


;; +============================================================================
;; |  Setup C Language Development Environment
;; +============================================================================

(setq c-default-style            "linux")
(setq c-basic-offset                   4)
(c-set-offset 'comment-intro           0)

;; Load relevant packages
(require 'cc-mode)
(require 'company)
(require 'flycheck)
(require 'projectile)
(require 'magit)

;; Additional recommended packages for C Language Development Environment
;;
;;   - Debugging
;;       gdb
;;
;;   - Code navigation
;;       counsel
;;       helm
;;
;;   - Build system integration
;;       cmake-mode
;;       compile
;;

;; Set up company-mode
(add-hook 'after-init-hook 'global-company-mode)

;; Set up flycheck
(projectile-mode +1)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

;; Set up Magit
(global-set-key (kbd "C-x g") 'magit-status)


;; +============================================================================
;; |  Setup PYTHON Development Environment
;; +============================================================================

;; ---------------------------------------------------------
;; Set Python3 as default
;; ---------------------------------------------------------
(setq py-python-command        "python3") ; Set Python 3 as default
(setq python-python-command    "python3") ; Set Python 3 as default
(setq python-shell-interpreter "python3") ; Set Python 3 as default

;; ---------------------------------------------------------
;; Enable elpy ([E]macs [L]isp [PY]thon) environment
;; ---------------------------------------------------------
(require 'elpy)
(elpy-enable)

;; ---------------------------------------------------------
;; Enable 'pep8' python syntax checker
;; WARNING: 'pep8' conflicts with 'blacken', enable only one
;; ---------------------------------------------------------
;;;(require 'py-autopep8)
;;;(add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)

;; +============================================================================
;; |  TERMINAL EMACS (Running on console text mode, not in GUI)
;; +============================================================================

;; The following will be configured only if running terminal Emacs

(unless (display-graphic-p)

  ;; Enable mouse.
  ;; Must have the 'gpm server' already installed and running for this to work.
  ;;(gpm-mouse-mode 1)
  )



;; +============================================================================
;; |  APPEARANCE (Themes)
;; +============================================================================

(load-theme 'tsdh-dark                 t)

;; ---------------------------------------------------------
;; Monokai
;; ---------------------------------------------------------
;; Port of the popular TextMate theme Monokai by Wimer Hazenberg.
;; The inspiration for the theme came from Bozhidar Batsov and his
;; Zenburn port and Sublime Text 2 which defaults to this color scheme.
;; ---------------------------------------------------------
;;;(load-theme 'monokai                t)

;; ---------------------------------------------------------
;; Gruvbox
;; ---------------------------------------------------------
;; Gruvbox is a retro groove color scheme for Emacs.
;; It is a port of the Vim version originally by Pavel Pertsev
;; ---------------------------------------------------------
;;;(load-theme 'gruvbox                t)    ; equivalent to 'gruvbox-dark-medium
;;;(load-theme 'gruvbox-dark-hard      t)
;;;(load-theme 'gruvbox-dark-medium    t)
;;;(load-theme 'gruvbox-dark-soft      t)
;;;(load-theme 'gruvbox-light-hard     t)
;;;(load-theme 'gruvbox-light-medium   t)
;;;(load-theme 'gruvbox-light-soft     t)

;; ---------------------------------------------------------
;; TangoTango
;; ---------------------------------------------------------
;; This is an emacs color theme based on the tango palette colors.
;; ---------------------------------------------------------
;;;(load-theme 'tangotango             t)

;; Wombat
;;; '(custom-enabled-themes    '(wombat))

;; +============================================================================
;; |  MISC BEHAVIOUR
;; +============================================================================

;; Auto-reload file if changed on disk *and* not-edited in buffer
(global-auto-revert-mode               t)

;; Enable server mode for 'emacsclient'
(server-start)

;; Inhibit display of Emacs initial screen
(setq inhibit-startup-message          t)

;; Enable line numbers globally
;;(setq global-display-line-numbers-mode t)

;; Enable line numbers in all programming modes
(add-hook 'prog-mode-hook 'display-line-numbers-mode)

;; Highlight parethesis pairs
(show-paren-mode                       t)

;; tab is 4 spaces
(setq tab-width                        4)

;; Move between open windows using [Shift]+[(ArrowKeys)]
(windmove-default-keybindings)

;; +============================================================================
;; |  CALENDAR
;; +============================================================================

;; Set to European date style **before** using any calendar or diary command.
(setq european-calendar-style t)

;; Week starts on Monday
(setq calendar-week-start-day 1)

;; Geographical location
(setq calendar-latitude      63.4)
(setq calendar-longitude     10.4)
(setq calendar-location-name "Trondheim, NO")

;; +==========================
;; | Display iso-week numbers
;; +==========================

;; Typeface height for iso-week numbers column
;;
;; Note: Using different typeface heights throws off the calendar alignment,
;;       so use the same height for the header and the column entries
;;
(setq calendar-iso-week-header-face-height     0.7     )
(setq calendar-iso-week-header-face-foreground "white" )
(setq calendar-iso-week-face-height            0.7     )
(setq calendar-iso-week-face-foreground        "salmon")


;; Typeface for iso-week header
;;
;;
(copy-face          'default                        'calendar-iso-week-header-face)
(set-face-attribute 'calendar-iso-week-header-face  nil
		    :foreground calendar-iso-week-header-face-foreground
		    :height     calendar-iso-week-header-face-height)

;; Typeface for iso-week numbers
;;
(copy-face          font-lock-constant-face  'calendar-iso-week-face)
(set-face-attribute 'calendar-iso-week-face  nil
		    :foreground calendar-iso-week-face-foreground
		    :height     calendar-iso-week-face-height)

;; Label 'Wk' for iso-week header
;;
(setq calendar-intermonth-header
      (propertize "Wk"
                  'font-lock-face
                  'calendar-iso-week-header-face))

;; Numbers for each week, following iso week numbering standard
;;
;; GnuEmacs 23.1 introduced the variable calendar-intermonth-text that can be
;; used for displaying ISO week numbers in the Calendar window. The code bellow
;; was adapted from this variable’s documentation:
;;
(setq calendar-intermonth-text
      '(propertize
        (format "%2d"
                (car
		 (calendar-iso-from-absolute
                 (calendar-absolute-from-gregorian (list month day year)))))
         'font-lock-face 'calendar-iso-week-face))

;; +========================
;; | Highlight weekend days
;; +========================

;; Source:
;; https://stackoverflow.com/questions/24008106/how-to-highlight-weekend-days-in-emacs-calendar

;; Set the font for highlighting weekend days in calendar
(copy-face 'font-lock-comment-face 'calendar-highlight-weekend-days-face)

;; Generate the month with the preferred weekend highlight colours
(defadvice calendar-generate-month
  (after highlight-weekend-days (month year indent) activate)
  "Highlight weekend days"
  (dotimes (i 31)
    (let ((date (list month (1+ i) year)))
      (if (or (= (calendar-day-of-week date) 0)
              (= (calendar-day-of-week date) 6))
		  (calendar-mark-visible-date date 'calendar-highlight-weekend-days-face)))))


;; +============================================================================
;; |  MODE-LINE
;; +============================================================================

;; ---------------------------------------------------------
;; Enable (line,column) display
;; ---------------------------------------------------------
(column-number-mode                    t)

;; ---------------------------------------------------------
;; Battery status
;; ---------------------------------------------------------
;; Display remaining battery% on mode-line
;; ---------------------------------------------------------
(require 'battery)

;; Conditional:
;; Ignore %Battery if there is no battery.
;; ----
;;;(when (and battery-status-function
;;;	   (not (string-match-p "N/A"
;;;             (battery-format "%B"
;;;                        (funcall battery-status-function)))))
;;;  (display-battery-mode t))

;; Unconditional:
;; Use default settings
;; ----
(display-battery-mode                  t)

;; ---------------------------------------------------------
;; Current time
;; ---------------------------------------------------------

;; Define a custom face for retro terminal look
;; ----
;;;(defface vapa_green-on-black_face
;;;  '((( (class color) )
;;;     :background "black"
;;;     :foreground "green"
;;;     :inherit    bold))
;;;  "Custom face 'green text on black background'."
;;;  )

;; This causes the current time in the mode line to be displayed in
;; a custom 'time-face' to make it stand out visually.
;; ----
;;;(setq my_display-time-string-forms
;;;      '((propertize (concat " " 24-hours ":" minutes " ")
;;;		    'face 'vapa_green-on-black_face)))

(setq
;;;display-time-day-and-date  t   ; Display date in addition to time
 display-time-24hr-format     t   ; Set time format to 24h
 )

;;;(display-time              t)  ; Display time and system load
(display-time-mode            t)  ; Display time and system load

;; +============================================================================
;; |  User-Defined init.el ends here
;; +============================================================================

;; *****************************************************************************
;; *****************************************************************************
;; *****************************************************************************
;; *****************************************************************************
;; *****************************************************************************

;; +============================================================================
;; |  !!! AUTOMATED SECTION !!!
;; +============================================================================
;; |  If you edit it by hand, you could mess it up, so be careful.
;; +============================================================================
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(moc astro-ts-mode magit-todos elpy lsp-ui use-package dap-mode company)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

