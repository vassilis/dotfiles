###############################################################
# i3wm - Natural scrolling script
# Inverts the scroll direction for mice and touchpads
#
# Source: https://www.iboard.cc/2020/andi/linux/i3/2020/03/20/i3xtermAndScrolling.html
#
# 1. Run 'xinput list' to list the devices
# 2. Replace the "Mouse" string with the device string
# 3. Add the following to your $HOME/.config/i3/config:
#
#     ## Scroll-wheel direction
#     exec --no-startup-id $HOME/repos/dotfiles/natural_scrolling
#
###############################################################

#################
# Generic Mouse
#################

id=`xinput list | grep -i "Mouse" | cut -d'=' -f2 | cut -d'[' -f1`
echo ""
echo "Generic Mouse: ID $id"

natural_scrolling_id=`xinput list-props ${id} | \
                      grep -i "Natural Scrolling Enabled (" \
                      | cut -d'(' -f2 | cut -d')' -f1`

echo "Natural scrolling ID ${natural_scrolling_id}"

xinput --set-prop $id ${natural_scrolling_id} 1

#######################
# Logitech Mouse M705
#######################

id=`xinput list | grep -i "Logitech M705" | cut -d'=' -f2 | cut -d'[' -f1`
echo ""
echo "Logitech M705: ID $id"

natural_scrolling_id=`xinput list-props ${id} | \
                      grep -i "Natural Scrolling Enabled (" \
                      | cut -d'(' -f2 | cut -d')' -f1`

echo "Natural scrolling ID ${natural_scrolling_id}"

xinput --set-prop $id ${natural_scrolling_id} 1

############
# Touchpad
############

id=`xinput list | grep -i "Touchpad" | cut -d'=' -f2 | cut -d'[' -f1`
echo ""
echo "Touchpad: $id"

natural_scrolling_id=`xinput list-props ${id} | \
                      grep -i "Natural Scrolling Enabled (" \
                      | cut -d'(' -f2 | cut -d')' -f1`

echo "Natural scrolling ID ${natural_scrolling_id}"

xinput --set-prop $id ${natural_scrolling_id} 1
