
# =============================================
# Extensions to default configuration
# =============================================

export BASE_DISTRO=ARCH
export ENVIRONMENT_TYPE=HOME

# Define custom files path
export PATH_CUSTOM=~/repos/dotfiles/bashrc/custom

# File & path with custom changes to the shell
# e.g. bashrc_custom.bash or zshrc_custom.sh
export SHELL_CUSTOM=$PATH_CUSTOM/bashrc_custom.bash

# Default bashrc file
export BASH_FILE=~/.bashrc

export CUSTOM_SHELL_ALREADY_INSTALLED="true"

# Start 'tmux',
# but only if 'tmux' or 'screen' are not already running
#
# Source:
# https://unix.stackexchange.com/questions/43601/how-can-i-set-my-default-shell-to-start-up-tmux
if command -v tmux &> /dev/null   &&
	[ -n "$PS1"             ] &&
	[[ ! "$TERM" =~ screen ]] &&
	[[ ! "$TERM" =~ tmux   ]] &&
	[ -z "$TMUX"            ];
then
    exec tmux

    # Now set the TERM value.
    # Options are:
    #     TERM=vt100
    #     TERM=vt220
    #     TERM=xterm
    #     TERM=xterm-256color
    #     TERM=screen          <---[ tmux default ]
    #     TERM=screen-256color
    #     TERM=linux

    # Advertise that we support 256 colours in tmux
    export TERM=screen-256color
fi

# Source the custom shell files
if [ -f $SHELL_CUSTOM ]; then
    . $SHELL_CUSTOM
else
    printf "/-----------------------------------------\n"
    printf " ERROR in '~/.bashrc'\n"
    printf "     ==> Could not locate '$SHELL_CUSTOM'\n"
    printf "\-----------------------------------------\n"
fi

# =============================================
# Extensions completed
# =============================================
