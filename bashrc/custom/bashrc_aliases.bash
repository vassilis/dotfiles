#!/bin/bash
alias rgrep='rgrep --color=auto'

alias year='ncal -Mwy'
alias month='ncal -Mw3'

alias ls='ls --color=auto'
alias ll='ls --color=auto -lF'
alias la='ls --color=auto -lFA'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'

alias ..='cd ../'

alias ip='ip -h -c'

alias dd='dd status=progress'

# .bashrc
alias sb='source ~/.bashrc'
alias vb='vim ~/.bashrc'

# software updates
alias ud='sudo apt update'
alias ug='sudo apt upgrade'
alias in='sudo apt install --only-upgrade'

alias lock_screen='gnome-screensaver-command --lock'
alias away='gnome-screensaver-command --lock'

# gnome
alias gnopen='gnome-open'

# Mutt email client
# Suppress console error output from corrupting the standard display
alias m='mutt 2>&1'

alias cddropbox='pushd /home/$USER/Dropbox'

# My public IP address
alias myip="curl http://ipecho.net/plain; echo"

# Edit root files using emacs.
# If an emacs server is already running then type "E <filename>"
# To start the emacs server type "M-x server-start" in emacs.
#alias "E=SUDO_EDITOR=\"emacsclient\" sudo -e"
#alias E='sudo -e'
alias E='sudoedit'

# Function: grep_here - Grep for string in Files
# Grep for a string inside files, starting 
grep_here ()
{
	grep -R -n --no-messages supress error messages "." -e $1
}

# Function: find_here - Find a file anywhere
find_here ()
{
	find `pwd` . ! -readable -prune -name $1
}
