#!/bin/bash

THIS_FILE="bashrc_prompt.sh"

#---------------------------------------------------------------------
# Load git command line prompt script
#---------------------------------------------------------------------
source_script $THIS_FILE $LINENO "$PATH_CUSTOM/git-prompt.sh"

#---------------------------------------------------------------------
# Enclose the sequence in '\[' ... '\]' characters
#---------------------------------------------------------------------
OPEN='\['
CLOSE='\]'

#---------------------------------------------------------------------
# (Re-)Define colours suitable for PS1
#
# Each colour must be enclosed in square brackets [ ... ] for PS1
# 'p_' stands for 'prompt_'
#---------------------------------------------------------------------
p_BOLD_TEXT_GREEN="$OPEN${BOLD_TEXT_GREEN}$CLOSE"
p_NORMAL_TEXT_RED="$OPEN${NORMAL_TEXT_RED}$CLOSE"
p_NORMAL_TEXT_CYAN="$OPEN${NORMAL_TEXT_CYAN}$CLOSE"
p_NORMAL_TEXT_PURPLE="$OPEN${NORMAL_TEXT_PURPLE}$CLOSE"
p_NORMAL_TEXT_YELLOW="$OPEN${NORMAL_TEXT_YELLOW}$CLOSE"
p_DEFAULT_COLOUR="$OPEN${DEFAULT_COLOUR}$CLOSE"


#---------------------------------------------------------------------
# Terminal prompt
#
# Local variables start with 'p_' to avoid naming conflicts and
# overwriting global variables
# 'p_' stands for 'prompt_'
#---------------------------------------------------------------------
p_USER="${p_NORMAL_TEXT_CYAN}\u"
p_AT="${p_DEFAULT_COLOUR}@"
p_HOST="${p_NORMAL_TEXT_PURPLE}\h"
p_COLON="${p_DEFAULT_COLOUR}:"
p_CURRENT_DIR="${p_BOLD_TEXT_GREEN}\w"
p_GIT_PROMPT="${p_NORMAL_TEXT_YELLOW}\$(__git_ps1)"
p_BLANK_SPACE=" "
p_NEW_LINE="\n"
#p_PROMPT_SYMBOL="${p_NORMAL_TEXT_RED}\$${p_DEFAULT_COLOUR}"
p_PROMPT_SYMBOL="${p_NORMAL_TEXT_RED}\$${p_DEFAULT_COLOUR}"
p_RESET_COLOURS="${p_DEFAULT_COLOUR}"

if [ "$BASE_DISTRO" = "DEBIAN" ]; then
    DEBIAN_PROMPT='${debian_chroot:+($debian_chroot)}'
else
    DEBIAN_PROMPT=""
fi

#PS1="\\
#${p_NEW_LINE}\\
#${DEBIAN_PROMPT}\\
#${p_USER}\\
#${p_AT}\\
#${p_HOST}\\
#${p_COLON_SEPARATOR}\\
#${p_BLANK_SPACE}\\
#${p_CURRENT_DIR}\\
#${p_GIT_PROMPT}\\
#${p_NEW_LINE}\\
#${p_PROMPT_SYMBOL}\\
#${p_BLANK_SPACE}"

PS1="\\
${p_NEW_LINE}${p_USER}${p_AT}${p_HOST}${p_COLON}\\
${p_BLANK_SPACE}\\
${p_CURRENT_DIR}\\
${p_GIT_PROMPT}\\
${p_NEW_LINE}${p_PROMPT_SYMBOL}${p_BLANK_SPACE}"
