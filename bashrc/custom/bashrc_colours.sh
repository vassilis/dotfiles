#!/bin/sh

#-------------------------------------------------
# Colour definitions
#-------------------------------------------------

# Start indicate the beginning of a colour command
START='\033['

# x;yzm - Indicates color code

# x - Indicates colour attribute
NORMAL='0'
BOLD='1'
DIM='2'
UNDERLINE='4'
BLINK='5'
REVERSE='7'
HIDDEN='8'

# y - Indicates text or background colour
TEXT='3'
BACKGR='4'

# zm - Indicates the actual colour
BLACK='0m'
RED='1m'
GREEN='2m'
YELLOW='3m'
BLUE='4m'
PURPLE='5m'
CYAN='6m'
WHITE='7m'

# Default colour resets all other prior colour changes
DEFAULT='00m'

#---------------------------------------------------------------------
# Foreground Colour definitions
#---------------------------------------------------------------------
export DEFAULT_COLOUR="$START$DEFAULT"

export NORMAL_TEXT_BLACK="$START$NORMAL;$TEXT$BLACK"
export NORMAL_TEXT_RED="$START$NORMAL;$TEXT$RED"
export NORMAL_TEXT_GREEN="$START$NORMAL;$TEXT$GREEN"
export NORMAL_TEXT_YELLOW="$START$NORMAL;$TEXT$YELLOW"
export NORMAL_TEXT_BLUE="$START$NORMAL;$TEXT$BLUE"
export NORMAL_TEXT_PURPLE="$START$NORMAL;$TEXT$PURPLE"
export NORMAL_TEXT_CYAN="$START$NORMAL;$TEXT$CYAN"
export NORMAL_TEXT_WHITE="$START$NORMAL;$TEXT$WHITE"
export NORMAL_BACKGR_BLACK="$START$NORMAL;$BACKGR$BLACK"
export NORMAL_BACKGR_RED="$START$NORMAL;$BACKGR$RED"
export NORMAL_BACKGR_GREEN="$START$NORMAL;$BACKGR$GREEN"
export NORMAL_BACKGR_YELLOW="$START$NORMAL;$BACKGR$YELLOW"
export NORMAL_BACKGR_BLUE="$START$NORMAL;$BACKGR$BLUE"
export NORMAL_BACKGR_PURPLE="$START$NORMAL;$BACKGR$PURPLE"
export NORMAL_BACKGR_CYAN="$START$NORMAL;$BACKGR$CYAN"
export NORMAL_BACKGR_WHITE="$START$NORMAL;$BACKGR$WHITE"

export BOLD_TEXT_GREY="$START$BOLD;$TEXT$BLACK"
export BOLD_TEXT_RED="$START$BOLD;$TEXT$RED"
export BOLD_TEXT_GREEN="$START$BOLD;$TEXT$GREEN"
export BOLD_TEXT_YELLOW="$START$BOLD;$TEXT$YELLOW"
export BOLD_TEXT_BLUE="$START$BOLD;$TEXT$BLUE"
export BOLD_TEXT_MAGENTA="$START$BOLD;$TEXT$PURPLE"
export BOLD_TEXT_CYAN="$START$BOLD;$TEXT$CYAN"
export BOLD_TEXT_WHITE="$START$BOLD;$TEXT$WHITE"
export BOLD_BACKGR_GREY="$START$BOLD;$BACKGR$BLACK"
export BOLD_BACKGR_RED="$START$BOLD;$BACKGR$RED"
export BOLD_BACKGR_GREEN="$START$BOLD;$BACKGR$GREEN"
export BOLD_BACKGR_YELLOW="$START$BOLD;$BACKGR$YELLOW"
export BOLD_BACKGR_BLUE="$START$BOLD;$BACKGR$BLUE"
export BOLD_BACKGR_MAGENTA="$START$BOLD;$BACKGR$PURPLE"
export BOLD_BACKGR_CYAN="$START$BOLD;$BACKGR$CYAN"
export BOLD_BACKGR_WHITE="$START$BOLD;$BACKGR$WHITE"

export BLINK_TEXT_GREY="$START$BLINK;$TEXT$BLACK"
export BLINK_TEXT_RED="$START$BLINK;$TEXT$RED"
export BLINK_TEXT_GREEN="$START$BLINK;$TEXT$GREEN"
export BLINK_TEXT_YELLOW="$START$BLINK;$TEXT$YELLOW"
export BLINK_TEXT_BLUE="$START$BLINK;$TEXT$BLUE"
export BLINK_TEXT_MAGENTA="$START$BLINK;$TEXT$PURPLE"
export BLINK_TEXT_CYAN="$START$BLINK;$TEXT$CYAN"
export BLINK_TEXT_WHITE="$START$BLINK;$TEXT$WHITE"
export BLINK_BACKGR_GREY="$START$BLINK;$BACKGR$BLACK"
export BLINK_BACKGR_RED="$START$BLINK;$BACKGR$RED"
export BLINK_BACKGR_GREEN="$START$BLINK;$BACKGR$GREEN"
export BLINK_BACKGR_YELLOW="$START$BLINK;$BACKGR$YELLOW"
export BLINK_BACKGR_BLUE="$START$BLINK;$BACKGR$BLUE"
export BLINK_BACKGR_MAGENTA="$START$BLINK;$BACKGR$PURPLE"
export BLINK_BACKGR_CYAN="$START$BLINK;$BACKGR$CYAN"
export BLINK_BACKGR_WHITE="$START$BLINK;$BACKGR$WHITE"

print_colour_demo ()
#---------------------------------------------------------------------
# Colour demo function
# Use it to prove the colour output works
#---------------------------------------------------------------------
{
	printf "%b" \
	   "$BOLD_TEXT_BLUE"                      "Hello "          \
	   "$BOLD_TEXT_RED"                       "World "          \
	   "$NORMAL_BACKGR_RED" "$TEXT_BOLD_CYAN" "!"               \
	   "$DEFAULT_COLOUR"                      " Back to normal" \
	   "$DEFAULT_COLOUR"                      "\n"
}
#---------------------------------------------------------------------
