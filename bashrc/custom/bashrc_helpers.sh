#!/bin/sh


error_missing_file_or_folder()
#---------------------------------------------------------------------
#
#---------------------------------------------------------------------
{
    local ERROR_FILE=$1               # Filename of caller to this function
    local ERROR_LINE=$2               # Line number causing this error
    local MISSING_FILE_OR_FOLDER=$3   # Object that is missing

    printf "${BOLD_TEXT_WHITE}$ERROR_FILE:$ERROR_LINE${DEFAULT_COLOUR}\n"
    printf "    ${NORMAL_TEXT_RED}ERROR${DEFAULT_COLOUR}"
    printf " => Missing '$MISSING_FILE_OR_FOLDER'\n"
    printf "\n"
}


source_script()
#---------------------------------------------------------------------
#
#---------------------------------------------------------------------
{
    local CALLER_FILE=$1   # Filename of caller to this function
    local CALLER_LINE=$2   # Line number calling this function
    local SCRIPT_FILE=$3   # Filename of script to be sourced

    if [ -e $SCRIPT_FILE ]; then
        source $SCRIPT_FILE
    else
	error_missing_file_or_folder "$CALLER_FILE" "$CALLER_LINE" "$SCRIPT_FILE"
    fi
}


run_binary()
#---------------------------------------------------------------------
#
#---------------------------------------------------------------------
{
    local CALLER_FILE=$1   # Filename of caller to this function
    local CALLER_LINE=$2   # Line number calling this function
    local BIN_FILE=$3      # Filename of binary to run

    if [ -e $BIN_FILE ]; then
        $BIN_FILE
    else
	error_missing_file_or_folder "$CALLER_FILE" "$CALLER_LINE" "$BIN_FILE"
    fi
}


append_to_PATH()
#---------------------------------------------------------------------
# Appends string $APPEND_PATH after current $PATH
#---------------------------------------------------------------------
{
    local CALLER_FILE=$1   # Filename of caller to this function
    local CALLER_LINE=$2   # Line number calling this function
    local APPEND_PATH=$3   # Path string to be appended to existing $PATH

    if [ -d ${APPEND_PATH} ] ; then
	export PATH="$PATH:$APPEND_PATH"
    else
	error_missing_file_or_folder "$CALLER_FILE" "$CALLER_LINE" "$APPEND_PATH"
    fi
}
