#!/bin/bash

#---------------------------------------------------------------------
# Useful local script variables
#---------------------------------------------------------------------
# The filename of this script
THIS_FILE="bashrc_custom.sh"

#---------------------------------------------------------------------
# Load colour definitions
#---------------------------------------------------------------------
COLOUR_DEFS=$PATH_CUSTOM/bashrc_colours.sh

if [ -f $COLOUR_DEFS ]; then
    . $COLOUR_DEFS
else
    printf "/-----------------------------------------\n"
    printf " ERROR in \'$THIS_FILE\'\n"
    printf "     ==> Could not locate '$COLOUR_DEFS'\n"
    printf "\-----------------------------------------\n"
fi


#---------------------------------------------------------------------
# Load helper functions
#---------------------------------------------------------------------
HELPERS=$PATH_CUSTOM/bashrc_helpers.sh

if [ -f $HELPERS ]; then
    . $HELPERS
else
    printf "/-----------------------------------------\n"
    printf " ERROR in \'$THIS_FILE\'\n"
    printf "     ==> Could not locate '$HELPERS'\n"
    printf "\-----------------------------------------\n"
fi

#---------------------------------------------------------------------
# Helper function
#
# Check that a variable has already been defined and is not NULL
#
# Based this StackOverflow question:
# https://stackoverflow.com/questions/3601515/how-to-check-if-a-variable-is-set-in-bash
#
# Note: Critical function, keep it here and do not move in 'bashrc_helpers' file
#---------------------------------------------------------------------
test_variable_is_not_NULL()
{
    local VARIABLE_NAME=$1    # Which variable is tested
    local CHECKED_BY_FILE=$2  # Who is testing the variable
    local CHECKED_AT_LINE=$3  # Where is the variable tested
    local DEFINITION_FILE=$4  # Where should the variable be defined

    # Variable Expansion/Indirection:
    # Get the value of the variable with this string name
    local VARIABLE_VALUE=${!VARIABLE_NAME}
    #echo $VARIABLE_NAME = \'$VARIABLE_VALUE\' # DIAGNOSTIC

    # Error if a variable has not been defined or if it has NULL value
    if [ -z ${VARIABLE_VALUE} ]
    then
	printf "\n" \
	printf " ${BOLD_TEXT_WHITE}$CHECKED_BY_FILE:$CHECKED_AT_LINE: " \
	printf " ${BLINK_TEXT_RED}CRITICAL ERROR: "
	printf " ${DEFAULT_COLOUR}Undefined variable \$$VARIABLE_NAME. \n"
	printf "    Define this variable in '$DEFINITION_FILE'\n"
	printf "\n"
    fi
}

#---------------------------------------------------------------------
# Check if we have defined all necessary variables
#---------------------------------------------------------------------
BASH_FILE=~/.bashrc
#test_variable_is_not_NULL "BASE_DISTRO"      $THIS_FILE $LINENO $BASH_FILE
#test_variable_is_not_NULL "ENVIRONMENT_TYPE" $THIS_FILE $LINENO $BASH_FILE
test_variable_is_not_NULL "SHELL_CUSTOM"     $THIS_FILE $LINENO $BASH_FILE
test_variable_is_not_NULL "PATH_CUSTOM"      $THIS_FILE $LINENO $BASH_FILE

#---------------------------------------------------------------------
# System editor & pager
#---------------------------------------------------------------------

# Edit system files easily with 'sudoedit' or 'sudo -e'
#
# Remember to add in /etc/sudoers file the line:
#
#   Defaults env_keep += "SUDO_EDITOR"
#
export SUDO_EDITOR='/usr/bin/emacs'


#export EDITOR='emacs -nw'
export EDITOR='/usr/bin/emacsclient'
export PAGER='/usr/bin/less'

#---------------------------------------------------------------------
# Python setup
#---------------------------------------------------------------------
export WORKON_HOME="$HOME/.virtualenvs"

#---------------------------------------------------------------------
# Git settings
#---------------------------------------------------------------------
export GIT_USER_ID=${USER}

#---------------------------------------------------------------------
# Load aliases
#---------------------------------------------------------------------
source_script $THIS_FILE $LINENO "$PATH_CUSTOM/bashrc_aliases.bash"

#---------------------------------------------------------------------
# Git autocomplete
#---------------------------------------------------------------------
#source_script $THIS_FILE $LINENO "$PATH_CUSTOM/git-completion.bash"
source /usr/share/bash-completion/completions/git 

#---------------------------------------------------------------------
# Setup ssh-agent so you don't have to type in the passphrase for
# each ssh transaction
#---------------------------------------------------------------------
source_script $THIS_FILE $LINENO "$PATH_CUSTOM/auto_launch_ssh_agent.bash"

#---------------------------------------------------------------------
# Setup command line prompt format
#---------------------------------------------------------------------
source_script $THIS_FILE $LINENO "$PATH_CUSTOM/bashrc_prompt.bash"

GIT_PROMPT="$PATH_CUSTOM/git-prompt.sh"

#---------------------------------------------------------------------
# Coloured GCC warnings and errors
#---------------------------------------------------------------------
export GCC_COLORS='error   = ${BOLD_RED}    : \
                   warning = ${BOLD_MAGENTA}: \
                   note    = ${BOLD_CYAN}   : \
                   caret   = ${BOLD_GREEN}  : \
                   locus   = 01             : \
                   quote   = 01'

#---------------------------------------------------------------------
# Toolchain for embedded ARM development
#---------------------------------------------------------------------
#export TOOLCHAIN=gcc-arm-none-eabi-4_9-2015q2
#export GCC_TOOLCHAIN_BIN_PATH=$HOME/bin/toolchains/${TOOLCHAIN}/bin
#export CROSS_COMPILE=${GCC_TOOLCHAIN_BIN_PATH}/arm-none-eabi-
#append_to_PATH $THIS_FILE $LINENO ${GCC_TOOLCHAIN_BIN_PATH}

#---------------------------------------------------------------------
# Arduino IDE
#---------------------------------------------------------------------
#append_to_PATH $THIS_FILE $LINENO ${HOME}/bin/arduino-ide

#---------------------------------------------------------------------
# eMail client
#---------------------------------------------------------------------
#export MAILCONF=$HOME/.mutt

#---------------------------------------------------------------------
# Go Language
#---------------------------------------------------------------------
#export GOPATH=$HOME/workspace
#append_to_PATH $THIS_FILE $LINENO ${GOPATH}/bin

#---------------------------------------------------------------------
# STM32CubeMX
#---------------------------------------------------------------------
#append_to_PATH $THIS_FILE $LINENO ${HOME}/bin/STMicroelectronics/STM32Cube/STM32CubeMX

#---------------------------------------------------------------------
# Salea Logic Software
#---------------------------------------------------------------------
#append_to_PATH $THIS_FILE $LINENO $HOME/bin/saleae_logic

#---------------------------------------------------------------------
# Build KDE
#---------------------------------------------------------------------
#append_to_PATH $THIS_FILE $LINENO $HOME/kde/src/kdesrc-build

#---------------------------------------------------------------------
# JHBuild tools for Gnome & GTK3 building
#---------------------------------------------------------------------
#append_to_PATH $THIS_FILE $LINENO $HOME/.local/bin

#---------------------------------------------------------------------
# Android & LineageOS development tools
# adb - Android Debug Bridge
#---------------------------------------------------------------------
#append_to_PATH $THIS_FILE $LINENO "$HOME/tools/adb-fastboot/platform-tools"

#---------------------------------------------------------------------
# Gnome keyring daemon - not for KDE
#---------------------------------------------------------------------
#eval `gnome-keyring-daemon --start`

#---------------------------------------------------------------------
# Note: Display banner for all users at first login, e.g.:
#     toilet -f smblock Welcome!
# goes into:
#     /etc/profile.d/display-banner.sh
# This display banner will be showin only at first login.
# It will not be shown in every new bash shell that opens.
#---------------------------------------------------------------------

# Start 'tmux',
# but only if 'tmux' or 'screen' are not already running
#
# Source:
# https://unix.stackexchange.com/questions/43601/how-can-i-set-my-default-shell-to-start-up-tmux

#######################################################################
## Disabled code.                                                     #
## To enable it, remove the single '#'-vertical-column                #
#######################################################################

#if command -v tmux &> /dev/null   &&
#	[ -n "$PS1"             ] &&
#	[[ ! "$TERM" =~ screen ]] &&
#	[[ ! "$TERM" =~ tmux   ]] &&
#	[ -z "$TMUX"            ];
#then
##    exec tmux
#    echo "Started tmux"
#
#    # Now set the TERM value.
#    # Options are:
#    #     TERM=vt100
#    #     TERM=vt220
#    #     TERM=xterm
#    #     TERM=xterm-256color
#    #     TERM=screen          <---[ tmux default ]
#    #     TERM=screen-256color
#    #     TERM=linux
#
#    if [ "$TERM" == screen ];
#    then
#	# Advertise that we support 256 colours in tmux
#	export TERM=screen-256color
#	echo "Changed TERM to 256color variant"
#    fi
#else
#    echo ""
#fi

#######################################################################
## End of disabled code                                               #
#######################################################################

#---------------------------------------------------------------------
# Python pipx configuration
#---------------------------------------------------------------------

#eval "$(register-python-argcomplete3 pipx)"
eval "$(register-python-argcomplete pipx)"

# Note:
#
# zsh
# --------
# To activate completions in zsh, first make sure compinit is marked
# for autoload and run autoload:
#
# autoload -U compinit && compinit
#
# Afterwards you can enable completions for pipx:
#
# eval "$(register-python-argcomplete pipx)"
#
# NOTE: If your version of argcomplete is earlier than v3, you may
# need to have bashcompinit enabled in zsh by running:
#
# autoload -U bashcompinit
# bashcompinit

#---------------------------------------------------------------------
# Rust - support 'cargo'
#---------------------------------------------------------------------
export PATH="$HOME/.cargo/bin/:$PATH"

#---------------------------------------------------------------------
# Info-tool (screenfetch, neofetch)
#---------------------------------------------------------------------
#run_binary $THIS_FILE $LINENO "/usr/bin/screenfetch"
run_binary $THIS_FILE $LINENO "/usr/bin/neofetch"
