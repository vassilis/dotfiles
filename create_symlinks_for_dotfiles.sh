#!/bin/bash

SYMLINK_PATH=$HOME/repos/dotfiles/symlinks

## FixMe: Path ~/.local/share/applications/ARCHLINUX/ is not valid for home folder.
##        However, this script will attempt to create symlinks inside ARCHLINUX
##        Therefore it is necessary to find a solution independent of the source
##        path containing ARCHLINUX or UBUNTU.
##        For now create the symlinks manually.

list=(
    config/clight-gui
    config/clight.conf
    config/dunst
    config/emacs
    config/htop
    config/i3
    config/kitty
    config/neofetch
    config/newsboat
    config/ranger
    config/regolith3
    config/sway
    config/weechat
    irssi
    links2
    local/share/applications/ARCHLINUX/mimeapps.list
    local/share/applications/ARCHLINUX/mimeinfo.cache
    local/share/applications/ARCHLINUX/teams_brave.desktop
    local/share/applications/ARCHLINUX/teams_chromium.desktop
    local/share/applications/ARCHLINUX/teams_google-chrome.desktop
    local/share/applications/ARCHLINUX/outlook_brave.desktop
    local/share/applications/ARCHLINUX/outlook_chromium.desktop
    local/share/applications/ARCHLINUX/outlook_google-chrome.desktop
    local/share/applications/ARCHLINUX/signal-desktop.desktop
    tmux.conf
    weather
    urxvt
    Xresources
)

# Create the folder for 'applications'.
# This will not exist in new installations'.
mkdir -p ~/.local/share/applications

# Make symlinks for all list entries
for ENTRY in "${list[@]}"
do
    # Simplify the paths & filenames
    SOURCE="$SYMLINK_PATH"/"$ENTRY"
    TARGET="$HOME"/."$ENTRY"

    # Remove any pre-existing files and folders
    rm -rf "$TARGET"

    # Create symlinks
    ln -s "$SOURCE" "$TARGET"
done

#
# Manual symlink actions
#

# .links => .links2
# links and links2 is (almost) the same text based browser
rm -rf $HOME/.links
ln -s $HOME/.links2 $HOME/.links

# Special note for 'links' & 'links2' text-based web browser:
#
# When editing the bookmarks file from inside the 'links' browser, the
# bookmarks file get deleted and recreated. This erases any symbolic link
# to a bookmarks file in a different folder, thus making it impossible
# to store only the bookmarks in the dotfiles repo.
#
# The solution is to make a symbolic link for the entire '.links2' folder
# and set git .ignore file to ignore files not related to bookmarks.
# This will stop the browsing history files, cookies files, etc, from
# being stored in the dotfiles repo.

