#!/bin/zsh

setopt PROMPT_SUBST
PROMPT='%n in ${PWD/#$HOME/~} >'
